A stupid-simple interface to the Qualys SSLLabs API.

Full API documentation is here: https://github.com/ssllabs/ssllabs-scan/blob/stable/ssllabs-api-docs.md

Author: Mike Patterson <mike.patterson@uwaterloo.ca>

Cribbed some code from https://github.com/TrullJ/ssllabs/blob/master/ssllabsscanner.py

Sample usage:

```python
>>> import qualysssl
>>> d = qualysssl.info_API()
>>> d
{u'newAssessmentCoolOff': 1000, u'messages': [u'This assessment service is provided free of charge by Qualys SSL Labs, subject to our terms and conditions: https://www.ssllabs.com/about/terms.html'], u'currentAssessments': 0, u'maxAssessments': 25, u'engineVersion': u'1.19.33', u'clientMaxAssessments': 25, u'criteriaVersion': u'2009j'}
>>> r = qualysssl.endpoint_data("myhost.nexus","127.0.0.1")
>>> r
{u'errors': [{u'message': u'Could not find assessment results for the host'}]}
>>> r = qualysssl.new_scan("myhost.nexus")
>>> r['grade']
>>> r['endpoints'][0]['grade']
u'T'
>>> r = qualysssl.endpoint_data("snowcrash.ca","192.99.10.99")
>>> r['grade']
u'T'
>>> 
```

Copyright: University of Waterloo, 2015
