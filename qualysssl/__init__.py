#!/usr/bin/env python

# cribbed original stuff off https://github.com/TrullJ/ssllabs/blob/master/ssllabsscanner.py
# see also https://github.com/ssllabs/ssllabs-scan/blob/stable/ssllabs-api-docs.md

# Author: Mike Patterson <mike.patterson@uwaterloo.ca>
# Copyright University of Waterloo, 2015, BSD 3-clause license
# tab indents 4 lyfe, screw you PEP

import requests
import time
import sys
import socket

def _requestAPI(path, payload={}, entry = "https://api.ssllabs.com/api/v2/"):
	url = entry + path 
	try:
		response = requests.get(url, params=payload)
	except requests.exception.RequestException as e:
		print e
		sys.exit(1)
	results = response.json()
	return results

def info_API():
	path = "info"
	results = _requestAPI(path)
	return results

def results_from_cache(host, publish = "off", startNew = "off", fromCache = "on", all = "done", maxage = '24'):
	path = "analyze"
	payload = {'host': host, 'publish': publish, 'startNew': startNew, 'fromCache': fromCache, 'all': all, 'maxAge': maxage}
	print "results_from_cache payload is\n\t{}".format(payload)
	results = _requestAPI(path, payload)
	return results

# Returns only from cache
def endpoint_data(host, ip, fromCache = "on"):
	path = "getEndpointData"
	payload = {'host': host, 's': ip, 'fromCache': fromCache}
	results = _requestAPI(path, payload)
	return results

# Tries repeatedly until it gets a result, can take some time
def new_scan(host, publish = "off", startNew = "on", all = "done", ignoreMismatch = "on"):
	path = "analyze"
	payload = {'host': host, 'publish': publish, 'startNew': startNew, 'all': all, 'ignoreMismatch': ignoreMismatch}
	results = _requestAPI(path, payload)
	payload.pop('startNew')
	while results['status'] != 'READY' and results['status'] != 'ERROR':
		time.sleep(30)
		results = _requestAPI(path, payload)
	return results			
