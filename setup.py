from distutils.core import setup
setup(
  name = 'qualysssl',
  packages = ['qualysssl'], # this must be the same as the name above
  version = '0.1',
  description = 'Python interface to the Qualys SSL API',
  author = 'Mike Patterson',
  author_email = 'mike.patterson@uwaterloo.ca',
  keywords = ['ssl', 'qualys'], # arbitrary keywords
  classifiers = [],
)
